#!/usr/bin/env perl

###############################################################################
#                                                                             #
#  Script Name: breach_status.sh                                              #
#  Creator: Gregory D. Horne (greg at gregoryhorne dot ca)                    #
#                                                                             #
###############################################################################


# Determine the breach status of a hospital emergency department.
# Count occurrences of 'safe' and 'breach' to determine the status of the    
# hospital emergency department. If there are between 1 and 3 breaches during
# the time period, set status to "Trouble." If there are more than 3 breaches  
# during the time period, set status to "Urgent Inspection." Otherwise, the   
# default status is "Safe."


sub breach_status($)
{
    # Check whether any data is available.
    $line = shift or die("no data");

    # Each record represents a single emergency department without any
    # identifying information about the name of the hospital or any unique
    # identifier for the particular emergency department. A record conists of
    # comma separated labels (safe or breach) each enclosed in single quotation
    # marks (') and optionally enclosed within one or more pair-matched
    # bracketed sublists ([ or ]). To simplify counting each occurrence label
    # is transformed to lowercase.

    chomp($line);
    $line =~ s/\[|\]|\'|\s//g;
    
    %count = ();
    foreach $str (split(/,/, lc($line))) { $count{$str}++; }

    if ($count{'breach'} > 0 && $count{'breach'} < 4) { return('Trouble'); }
    if ($count{'breach'} > 3) { return('Urgent Inspection'); }
    return('Safe');
}


# Report the hospital emergency department status.

$file = "./datafile";
open($fh, "<", $file);

print("\n\nHospital Emergency Department Report\n\nHospital\tBreach Status\n");
$hospital = 0;
while ($line = <$fh>) {
    print($hospital = $hospital + 1, "\t\t", breach_status($line), "\n");
}
close($fh);

exit(0);
