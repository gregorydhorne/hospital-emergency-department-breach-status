#!/usr/bin/env sh

###############################################################################
#                                                                             #
#  Script Name: breach_status.sh                                              #
#  Creator: Gregory D. Horne (greg at gregoryhorne dot ca)                    #
#                                                                             #
###############################################################################


# Determine the breach status of a hospital emergency department.
# Count occurrences of 'safe' and 'breach' to determine the status of the    
# hospital emergency department. If there are between 1 and 3 breaches during
# the time period, set status to "Trouble." If there are more than 3 breaches  
# during the time period, set status to "Urgent Inspection." Otherwise, the   
# default status is "Safe."

breach_status()
{
    # Check whether any data is available.
    if [ -z "${1}" ]
    then
        echo "no data"
        exit 1
    fi

    # Each record represents a single emergency department without any
    # identifying information about the name of the hospital or any unique
    # identifier for the particular emergency department. A record conists of
    # comma separated labels (safe or breach) each enclosed in single quotation
    # marks (') and optionally enclosed within one or more pair-matched
    # bracketed sublists ([ or ]). To simplify counting each occurrence label
    # is transformed to lowercase.

    ###########################################################################
    # Explanatory Note: For those interested in regular expression pattern
    # matching, a brief overview of the pattern in the gsub() function of awk
    # is provided.
    #
    # The first argument passed to the global substitution function (gsub)
    # is a pattern consisting of a left bracket ([), a right bracket (]), a
    # single quotation mark (') denoted in octal format as \047 (4 * 8 + 7 = 39
    # decimal) in the ASCII character set, a blank space ( ) denoted in octal
    # format as \040 (4 * 8 + 0 = 32 decimal) in the ASCII character set. The
    # leading backslash (\) is necessary to signify the brackets are literal
    # brackets, not regular expression grouping markers as is ordinariy the
    # case. Similarly, the leading blackslash and zero (\0) signify an octal
    # number.
    #
    # In plain language the regular expression (regex) is interpreted as
    # follows:
    #
    # If the current line ($0) of text passed to the breach_status function
    # as its first argument (${1} or more simply $1) contains one or more of the
    # characters ([, ], ', or a space), then replace each occurrence with the
    # value passed as the second argument to the gsub() function, in this case
    # an empty character string ("").
    #
    # Futher, the BEGIN and END stanzas are equivalent to pre-condition and
    # post-condition statements in various programming languages. These are only
    # executed once per inocation of the awk statement regardless of the number
    # of lines (records). The breach_status function only receives a
    # single line of text each time it is called so awk receives a single line.
    # The bracketed stanza between BEGIN and END is executed for each line of
    # text. The third argument ($0) to the gsub() function can be omitted when
    # it is $0 (the entire line of text). The for-loop iterates over the $0
    # extracting the next value separated by the field separator (FS=","), a
    # comma, and increments the occurrence count for that token, either
    # 'safe' or 'breach' (no quotation marks). The status is determined in the
    # END stanza.
    #
    # In production systems this level of explanation is unwarranted. The
    # details are included solely for educational purposes at the suggestion of
    # the Clinical Developer's Club.
    ###########################################################################

    status=$(echo ${1} | \
    awk \
        'BEGIN { FS = "," }
         { gsub(/\[|\]|\047|\040/, "", $0); 
           for(i=1;i<=NF;i++) a[tolower($i)]++ }
         END { if (a["breach"] > 0 && a["breach"] < 4) print("Trouble")
               else if (a["breach"] > 3) print("Urgent Inspection")
               else print("Safe") }')
}


# Report the hospital emergency department status.

printf "\n\nEmergency Department Status\n\n"
printf "Hospital\tBreach Status\n"

record=0
while read line
do
    record=$((record + 1)) 
    breach_status "${line}"
    printf "%8s\t%s\n" $record "${status}"
done < ./datafile

exit 0
